#!/bin/sh

set -ue
. $(dirname $0)/functions

LXC_NAME=${LXC_NAME:=tinytest}
[ "$(id -u)" = 0 ] || LXC_NAME=${LXC_NAME}-$USER

aa_namespace=${aa_namespace:=lxc-apertis-tiny}
imageshost=${imageshost:=images.apertis.org}

get_opts "$@"

# Cleanup container if needed
container_reset

container_start_bg && container_os_wait

RESULT=0
TESTRESULT=0

DEBUGOUT="/dev/null" #"&1" to get output in console

MUSTFAIL()
{
	if container_exec $* >$DEBUGOUT 2>&1; then
#	if container_exec $* ; then
		echo $* unexpected pass.
        TESTRESULT=1
	fi
}

MUSTPASS()
{
	if ! container_exec $* >$DEBUGOUT 2>&1; then
#	if ! container_exec $*; then
		echo $* unexpected failure.
        TESTRESULT=1
	fi
}

CHECK_RESULT()
{
    if [ $TESTRESULT -eq 0 ]; then
        echo "container-${container_type}-$1:" "pass"
    else
        echo "container-${container_type}-$1:" "fail"
        # Do not want to exit here -- need to proceed with the rest
        RESULT=1
    fi
    # Reset the test status
    TESTRESULT=0
}

# Test if all needed binaries are in place and able to run
test_name=aa_binaries_access
BINARIES="ls touch mkdir head cat cp mknod"
for binary in $BINARIES; do
	MUSTPASS test "$binary" = "$(basename $(which $binary))"
	MUSTPASS $binary --version
done

#Specific test for grep since it is implemented by busybox
MUSTPASS test "grep" = "$(basename $(which grep))"
MUSTPASS grep --help

MUSTPASS which aa-enabled
MUSTPASS test `aa-enabled` = "Yes"
CHECK_RESULT $test_name

# Accessing sensitive informations in /sys
test_name=aa_sys_access
MUSTPASS ls /sys/kernel/security/
MUSTPASS ls /sys/kernel/security/apparmor/
MUSTPASS ls /sys/kernel/security/apparmor/policy/namespaces/
MUSTFAIL ls /sys/kernel/debug
CHECK_RESULT $test_name

# Setting kernel parameters by writing to /sys
test_name=aa_sys_gpio_access
MUSTPASS ls /sys/class/gpio/
MUSTFAIL touch /sys/class/gpio/export
CHECK_RESULT $test_name

# Accessing memory outside of its realm
test_name=aa_mem_access
MUSTPASS ls /proc/
for m in /proc/kmem /proc/mem /dev/mem /proc/kcore
do
    MUSTFAIL head -c 1 $m
done
CHECK_RESULT $test_name

# Writing to the base container rootfs except /var and /run
test_name=aa_rootfs_access
for path in /var /run /tmp ; do
	MUSTPASS touch $path
	MUSTPASS touch $path/aa-test-file
	MUSTPASS mkdir $path/aa-test-folder
done
for path in $(container_exec ls / | grep -v -E "var|run|tmp") ; do
	MUSTFAIL touch /$path/aa-test-file
	MUSTFAIL mkdir /$path/aa-test-folder
#	MUSTFAIL touch $path
done
CHECK_RESULT $test_name

# reading and writing outside of the rootfs
# From the container, this is controlled by the container filesystem
# To test it would require to identify a host file from the container
# And make sure it cannot be changed

# Accessing devices outside of its realm mounting devpts
test_name=aa_dev_access
MUSTPASS ls -l /dev
MUSTPASS ls -l /dev/console
MUSTFAIL mknod /dev/console.fake c 5 1
MUSTPASS ls -l /dev
MUSTFAIL mknod /dev/sda1.fake b 8 1
MUSTPASS ls -l /dev
MUSTFAIL cp /dev/null /dev/ptmx
MUSTFAIL cp /dev/null /dev/pts/0
CHECK_RESULT $test_name

# Escaping the container by loading kernel modules test
test_name=aa_prevent_module_load

# Need to start tests with updated configuration
lxc-stop -k -n $LXC_NAME

# Add mapping of modules and utilities absent in container
cat >>$lxc_config <<E_O_F
lxc.mount.entry = /lib/modules lib/modules none ro,bind,create=dir
E_O_F

container_start_bg

MUSTPASS busybox lsmod
MUSTPASS ls /sys/module

module="$(
busybox lsmod | while read modname size used; do
    if [ "${used}" = "0" ]; then
        echo ${modname}
        break
    fi
done
)"

modprobe -r ${module}
MUSTFAIL busybox modprobe ${module}
modprobe ${module}

MUSTFAIL busybox modprobe -r ${module}
CHECK_RESULT $test_name

exit $RESULT
