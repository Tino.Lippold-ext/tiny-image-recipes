{{- $architecture := or .architecture "armhf" -}}
{{- $type := or .type "rfs" -}}
{{- $mirror := or .mirror "https://repositories.apertis.org/apertis/" -}}
{{- $suite := or .suite "v2023" -}}
{{ $timestamp := or .timestamp "" }}
{{- $filename := or .filename (printf "isolation_%s-%s-%s" $type $suite $architecture) -}}
{{- $ipk := or .ipk "false" -}}
{{- $ipk_target := or .ipk_target "/opt/containers/apertis_$type" -}}
{{- $maintainer := or .maintainer "Apertis team" -}}

architecture: {{ $architecture }}

actions:
  - action: debootstrap
    suite: {{ $suite }}
    components:
      - target
    mirror: {{ $mirror }}
    variant: minbase
    keyring-package: apertis-archive-keyring
    keyring-file: keyring/apertis-archive-keyring.gpg
    merged-usr: false

  # Add image version information
  - action: run
    description: "Setting up image version metadata"
    chroot: true
    script: scripts/setup_image_version.sh apertis {{ $suite }} '{{ $timestamp }}' collabora {{ $type }}

  # Extend apt sources list
  - action: run
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} target 

  # Add type-based apt sources list
  {{ if eq $type "sysroot" }}
  - action: run
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} development

  - action: run
    chroot: true
    script: scripts/replace-tar-coreutils-for-build.sh
  {{ end }}

  - action: apt
    description: "Core packages"
    packages:
      - apt-transport-https

  - action: apt
    description: "Core libraries"
    packages:
      - libexpat1
      - libdlt2
      - libglib2.0-0

  - action: apt
    description: "AppArmor packages"
    packages:
      - apparmor
      - chaiwala-apparmor-profiles

  - action: apt
    description: "Dbus packages"
    packages:
      - dbus
      - dbus-user-session
      - dbus-x11

  {{ if eq $type "sysroot" }}
  - action: apt
    description: "Development packages"
    packages:
      - gir1.2-secret-1
      - libasound2-dev
      - libbluetooth-dev
      - libcurl4-nss-dev
      - libdlt-dev
      - libffi-dev
      - libgirepository1.0-dev
      - libglib2.0-0-dbgsym
      - libglib2.0-dev
      - libgstreamer-plugins-base1.0-dev
      - libgstreamer1.0-dev
      - libgudev-1.0-dev
      - libgupnp-1.6-dev
      - libhyphen-dev
      - libicu-dev
      - libjpeg-dev
      - libnotify-dev
      - libpango1.0-dev
      - libpng-dev
      - libpoppler-glib-dev
      - libproxy-dev
      - libpulse-dev
      - libsecret-1-0
      - libsecret-1-dev
      - libsoup2.4-dev
      - libsqlite3-dev
      - libsystemd-dev
      - libudev-dev

  - action: apt
    description: "Development tools"
    packages:
      - fakeroot
  {{ if eq $architecture "armhf" }}
      - linux-headers-armmp
  {{ else }}
      - linux-headers-{{$architecture}}
  {{ end }}
      - lsb-release
      - pkg-config
      - symlinks
      - sudo
  {{ end }}

  - action: overlay
    source: overlays/arch-platform

  {{ if eq $type "rfs" }}
  - action: overlay
    description: "Copy custom AppArmor profiles"
    source: overlays/apparmor

  - action: run
    description: "Purge packages not needed at runtime"
    chroot: true
    script: scripts/purge-unneeded-packages.sh

  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{$filename}}.pkglist.gz"


  - action: run
    chroot: true
    script: scripts/create-mtab-symlink.hook.sh

  - action: run
    chroot: true
    script: scripts/create-apparmor-cache-symlink.hook.sh

  - action: run
    chroot: true
    script: scripts/disable_services.sh

  - action: run
    description: "Set default boot target"
    chroot: true
    command: systemctl set-default multi-user.target

  {{ end }}

  {{ if eq $type "sysroot" }}
  - action: run
    chroot: true
    script: scripts/setup_user.sh

  - action: run
    chroot: true
    script: scripts/add_user_to_groups.sh

  - action: run
    chroot: true
    script: scripts/check_sudoers_for_admin.sh

  - action: run
    chroot: true
    script: scripts/generate_openssh_keys.sh

  - action: run
    description: "Generate locales"
    chroot: true
    script: scripts/generate_locales.sh

  - action: run
    chroot: true
    command: symlinks -rc /

  - action: run
    chroot: true
    command: echo "include ld.so.conf.d/*.conf" > /etc/ld.so.conf
  {{ end }}

  {{ if eq $type "rfs" }}
  # Do cleanup as a last preparation step
  - action: run
    description: "Purge directories and files not needed at runtime"
    chroot: true
    script: scripts/purge-unneeded-files.sh

  - action: run
    description: "/dev cleanup"
    chroot: false
    script: scripts/dev_cleanup.sh

  # This action must be placed just before tarball creation
  - action: run
    description: "List files"
    chroot: false
    command: find "$ROOTDIR" -printf '/%P %s %M %u %g\n' | sort | gzip > "${ARTIFACTDIR}/{{$filename}}.filelist.gz"
  {{ end }}

  - action: pack
    compression: gz
    file: {{$filename}}.tar.gz

  {{ if ne $ipk "false"}}
  - action: run
    description: "Generate IPK package"
    chroot: false
    script: scripts/create_container_ipk.sh  -m "{{$maintainer}}" -t "{{$ipk_target}}" -v "{{$suite}}" -o "{{$filename}}.ipk"
  {{ end }}
