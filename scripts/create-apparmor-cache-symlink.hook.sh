#!/bin/sh

set -e

# By default apparmor tries to cache profiles under /etc/apparmor.d/cache which is ro.
# Lets create a /etc/apparmod.d/cache symlink to /var/cache/apparmor instead.

mkdir -p /var/cache/apparmor
rm -rf /etc/apparmor.d/cache
ln -sf /var/cache/apparmor /etc/apparmor.d/cache
