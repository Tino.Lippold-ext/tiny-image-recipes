#!/bin/sh

set -eu

prefix="/lib/systemd/system"

services='
dev-mqueue.mount
console-getty.service
container-getty@.service
systemd-journald-audit.socket
systemd-sysusers.service
'
for service in $services; do
    find $prefix -name $service \( -type l -o -type f \) -delete
done
