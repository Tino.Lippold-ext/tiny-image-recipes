#!/bin/sh

set -e

# Generate openssh keys
echo "I: Regenerate SSH keys"
if dpkg -l openssh-server | tail -n 1 | grep -q ^ii
then
	dpkg-reconfigure openssh-server
else
	echo "W: openssh-server not found. Avoiding key generation."
fi
