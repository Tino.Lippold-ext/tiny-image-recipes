#!/bin/sh

# Defaults
rootfs=""
version=""
maintainer=""
ipkfile=""
target=""

help(){
    cat <<E_O_F
$0 <-v|--version version> <-m|--maintainer name> <-o|--out name> <-t|--target path> [rootfs]
there:
  -v -- provide version of package
  -m -- set maintainer\'s name and mail
  -o -- output filename
  -t -- target path with rootfs in package
  rootfs -- filesystem structure to be used for container. Variable "ROOTDIR" could be used instead.
E_O_F
}

# if the script is called in Debos
[ -z "${ROOTDIR}" ] || rootfs="${ROOTDIR}"

# Parse command line
opts=$(getopt -o "m:v:o:t:" -l "main:,version:,out:,target:" -- "$@")
eval set -- "$opts"

while [ $# -gt 0 ]; do
    case $1 in
        -m|--maintainer) maintainer="$2"; shift 2;;
        -v|--version) version="$2"; shift 2;;
        -o|--out) ipkfile="$2"; shift 2;;
        -t|--target) target="$2"; shift 2;;
        --) shift; break;;
        *) ;;
    esac
done

# override if rootfs provided via commandline
[ -z "$1" ] || rootfs="$1"

# by default ipk is generated in current directory
# in Debos if ARTIFACTDIR is set -- all resulting binaries should be there
[ -z "$ARTIFACTDIR" ] || ipkfile="$ARTIFACTDIR/$ipkfile"

if [ -z "$version" -o -z "$rootfs" -o -z "$maintainer" -o -z "$ipkfile" -o -z "$target" ]; then
    help
    exit 1
fi

set -eu

PREFIX=$(mktemp -d /tmp/ipk.XXXXX)

cleanup(){
    rm -rf $PREFIX
}

trap cleanup EXIT

create_control(){
    cat > ${PREFIX}/control <<EOF
Package: isolation_rfs
Version: ${version}
Description: rootfs for containers
Section: base
Source: N/A
Priority: optional
Maintainer: ${maintainer}
Architecture: all
EOF
    tar -C ${PREFIX} -czf ${PREFIX}/control.tar.gz control
    rm -f ${PREFIX}/control
}

create_debian_binary(){
    echo "2.0" > ${PREFIX}/debian-binary
}

create_data(){
    # pack rootfs for ipk to be installed in target directory
     tar --transform="s%^${rootfs}%${target}%" -P -czf ${PREFIX}/data.tar.gz ${rootfs}
}

create_ipk(){
    ar -crf -D "${ipkfile}" ${PREFIX}/debian-binary ${PREFIX}/control.tar.gz ${PREFIX}/data.tar.gz
}

create_data
create_control
create_debian_binary
create_ipk
