image: $DOCKER_IMAGE

######
# variables
#
# The main variables to customize the built artifacts

variables:
  osname: apertis
  release: "v2025dev2"
  upload_host: archive@images.apertis.org
  upload_port: '7711'
  upload_root_main: /srv/images/public
  upload_root_test: /srv/images/test/$CI_COMMIT_REF_SLUG
  image_url_prefix_main: https://images.apertis.org
  image_url_prefix_test: https://images.apertis.org/test/$CI_COMMIT_REF_SLUG
  test_repo_url: https://gitlab.apertis.org/tests/apertis-test-cases.git
  recipe: tiny-connectivity-container.yaml
  mirror: https://repositories.apertis.org/apertis/

default:
  retry:
    max: 1
    when:
      # sometimes the jobs takes more time than expected due to slow network traffic or other unrelated issues
      # some other times the fakemachine startup gets stuck while mounting filesystems and drops into the emergency shell
      # https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/jobs/332907
      #   [FAILED] Failed to mount /etc/alternatives.
      #   See 'systemctl status etc-alternatives.mount' for details.
      #   [DEPEND] Dependency failed for Local File Systems.
      - job_execution_timeout
      # in some cases UML seems behave surprisingly, leading to crashes up in the stack, for instance in the ostree
      # locking from debos
      # https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/jobs/333401
      #   (process:1593): OSTree-CRITICAL **: 16:03:48.541: pop_repo_lock: assertion 'lock_table != NULL' failed
      #   (process:1593): GLib-CRITICAL **: 16:03:48.542: g_propagate_error: assertion 'src != NULL' failed
      #   ERROR:/usr/local/go/src/github.com/sjoerdsimons/ostree-go/pkg/glibobject/glibobject.go.h:6:_g_error_get_message: assertion failed: (error != NULL)
      #   SIGABRT: abort
      - script_failure

stages:
  - build-env
  - preparation
  - build
  - upload
  - submit tests

######
# Permissions fixup
#
# GitLab uses a too-lax umask when checking out the repositories, which leads
# to root-owned  world-writable files being put in the images.
#
# The image-builder container defaults to umask 022 which would only affects
# the w bit, so reset it for group and other users.
#
# See https://gitlab.com/gitlab-org/gitlab-runner/issues/1736
before_script: &gitlab_permissions_fixup
  - chmod -R og-w .

.lightweight:
  tags:
    - lightweight

######
# stage: preparation

debos-dry-run:
  stage: preparation
  extends:
    - .lightweight
  script:
    - debos --print-recipe --dry-run ${recipe}

prepare-build-env:
  stage: build-env
  extends:
    - .lightweight
  image: debian:bullseye-slim
  before_script:
    - apt update && apt install -y --no-install-recommends
        ca-certificates
        curl
        jq
        skopeo
  script:
    # multiline variables have CRLF line endings, likely because
    # x-www-form-urlencoded and form-data mandate them, so let's get rid of the
    # extra `\r` and ensure there's a line terminator at the end of the file
    # (by appending nothing, but it works)
    - test -n "$BUILD_ENV_OVERRIDE" && sed -i 's/\r$// ; $a\'  "$BUILD_ENV_OVERRIDE" && . "$BUILD_ENV_OVERRIDE"
    - PIPELINE_VERSION=${PIPELINE_VERSION:-$(date +%Y%m%d.%H%M)}
    - DOCKER_IMAGE_BASE=$CI_REGISTRY/infrastructure/${osname}-docker-images/${release}-image-builder
    - DOCKER_IMAGE=${DOCKER_IMAGE:-$DOCKER_IMAGE_BASE@$(skopeo --creds "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" inspect docker://$DOCKER_IMAGE_BASE | jq -r .Digest)}
    - mkdir -p a/$PIPELINE_VERSION/meta
    - BUILD_ENV=a/$PIPELINE_VERSION/meta/build-env-$CI_PROJECT_NAME.txt
    - echo "PIPELINE_VERSION=$PIPELINE_VERSION" | tee -a $BUILD_ENV
    - echo "DOCKER_IMAGE=$DOCKER_IMAGE" | tee -a $BUILD_ENV
    - echo "RECIPES_COMMIT=$CI_COMMIT_SHA" | tee -a $BUILD_ENV
    - echo "RECIPES_URL=$CI_PROJECT_URL"  | tee -a $BUILD_ENV
    - echo "PIPELINE_URL=$CI_PIPELINE_URL" | tee -a $BUILD_ENV
    - APT_SNAPSHOT_LATEST_URL="${mirror%/}/dists/${release}/snapshots/latest.txt"
    - echo "Fetching $APT_SNAPSHOT_LATEST_URL"
    - APT_SNAPSHOT_LATEST=$(curl --fail --silent --show-error "$APT_SNAPSHOT_LATEST_URL")
    - APT_SNAPSHOT=${APT_SNAPSHOT:-$APT_SNAPSHOT_LATEST}
    - echo "APT_SNAPSHOT=$APT_SNAPSHOT" | tee -a $BUILD_ENV
    - |
      case "$CI_COMMIT_REF_NAME" in
        apertis/*)
          upload_root=$upload_root_main
          image_url_prefix=$image_url_prefix_main
          source_wip=false
          ;;
        *)
          upload_root=$upload_root_test
          image_url_prefix=$image_url_prefix_test
          source_wip=true
          ;;
      esac
    - echo "IMAGE_URL_PREFIX=${IMAGE_URL_PREFIX:-$image_url_prefix}" | tee -a $BUILD_ENV
    - echo "UPLOAD_ROOT=$upload_root" | tee -a $BUILD_ENV # cannot be overridden to avoid overwriting previous uploads
    - echo "SOURCE_WIP=$source_wip" | tee -a $BUILD_ENV
    - test -n "$BUILD_ENV_OVERRIDE" && diff --color=always -U 10 "$BUILD_ENV_OVERRIDE" "$BUILD_ENV" || true
  artifacts:
    expire_in: 1d
    paths:
      - a/*/meta/build-env-$CI_PROJECT_NAME.txt
    reports:
      dotenv: a/*/meta/build-env-$CI_PROJECT_NAME.txt

######
# stage: build
#
# These variables are meant to be used when instantiating the templates
# included above to choose the desired ospack flavours

.ospack-build:
  stage: build
  script:
    - mkdir -p a/${PIPELINE_VERSION}/${architecture}/isolation-${type}
    - cd a/${PIPELINE_VERSION}/${architecture}/isolation-${type}
    - debos ${debosarguments}
        --show-boot
        -t type:${type}
        -t architecture:${architecture}
        -t suite:${release}
        -t timestamp:${PIPELINE_VERSION}
        -t ipk:${ipk}
        -t filename:isolation_${type}-${release}-${architecture}-${PIPELINE_VERSION}
        $CI_PROJECT_DIR/${recipe}
  artifacts:
    expire_in: 1d
    paths:
      - a/*/*/*/isolation_*
  needs:
    - debos-dry-run
    - prepare-build-env

######
# stage: upload

.artifacts-upload:
  stage: upload
  extends:
    - .lightweight
  variables:
    target: daily/$release
    upload_dest: ${upload_host}:${UPLOAD_ROOT}
    source: a/$PIPELINE_VERSION
  script:
    # see https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
    - eval $(ssh-agent -s)
    - chmod 600 "$ARCHIVE_SECRET_FILE"
    - ssh-add "$ARCHIVE_SECRET_FILE"
    - echo Uploading ${source} to ${upload_dest}/${target}/
    - ssh -oStrictHostKeyChecking=no -p ${upload_port} ${upload_host} mkdir -p ${UPLOAD_ROOT}/${target}/
    - rsync -e "ssh -p ${upload_port} -oStrictHostKeyChecking=no" -aP ${source} ${upload_dest}/${target}/
  rules:
    - if: '$CI_MERGE_REQUEST_ID'
      when: never
    - if: '$ARCHIVE_SECRET_FILE == null'
      when: never
    - when: on_success

######
# stage: submit tests

.submit-tests:
  stage: submit tests
  extends:
    - .lightweight
  variables:
    test_repo_branch: $osname/$release
    deployment: lxc
    source_commit: $CI_COMMIT_SHA
    source_job: $CI_JOB_URL
    source_pipeline: $CI_PIPELINE_URL
    source_project: $CI_PROJECT_URL
    source_ref: $CI_COMMIT_REF_NAME
    # source_wip:true means that we skip creating tasks on Phab on test
    # failures, and that in general the results should not impact the overall
    # health of the project
    source_wip: $SOURCE_WIP
  script:
    - ospack=${IMAGE_URL_PREFIX}/daily/${release}/${PIPELINE_VERSION}/${architecture}/isolation-${type}/isolation_${type}-${release}-${architecture}-${PIPELINE_VERSION}.tar.gz
    - git clone "${test_repo_url}" -b "${test_repo_branch}" tests/
    - cd tests/
    - env --null | sort -z | tr '\0' '\n'
    - set -xu
    - ./generate-jobs.py
        -d lava/devices.yaml
        --config ../lavatests/config.yaml
        --release ${release}
        --arch ${architecture}
        --board ${board}
        --osname ${osname}
        --deployment ${deployment}
        --type tiny-lxc
        --date ${PIPELINE_VERSION}
        --name isolation_${type}-${release}-${architecture}-${PIPELINE_VERSION}.tar.gz
        --output-dir ../
        --skip-sanity-checks
        -t "source_commit:${source_commit}"
        -t "source_job:${source_job}"
        -t "source_pipeline:${source_pipeline}"
        -t "source_project:${source_project}"
        -t "source_ref:${source_ref}"
        -t "source_wip:${source_wip}"
        -t "ospack:${ospack}"
        --verbose
    - ./lava-submit.py
        --jobid-file ../lava-jobs.json
        --callback-secret "${QA_REPORT_TOKEN}"
        --callback-url https://qa.apertis.org/
        -c ${LQA_CONFIG} submit
        ../job-*.yaml
  artifacts:
    paths:
      - lava-jobs.json
      - job-*.yaml
  rules:
    - if: '$CI_MERGE_REQUEST_ID'
      when: never
    - if: '$LQA_CONFIG == null'
      when: never
    - when: on_success

######
# architectures
.armhf:
  variables:
    architecture: armhf
    board: uboot

.arm64:
  variables:
    architecture: arm64
    board: uboot

.amd64:
  variables:
    architecture: amd64
    board: uefi

######
# ospack types

.rfs:
  variables:
    type: rfs
    ipk: "true"

.sysroot:
  variables:
    type: sysroot
    ipk: "false"
    debosarguments:
      --scratchsize 10G

######
# stage: ospack build

build-armhf-rfs:
  extends:
    - .ospack-build
    - .armhf
    - .rfs

build-arm64-rfs:
  extends:
    - .ospack-build
    - .arm64
    - .rfs

build-amd64-rfs:
  extends:
    - .ospack-build
    - .amd64
    - .rfs

build-armhf-sysroot:
  extends:
    - .ospack-build
    - .armhf
    - .sysroot

build-arm64-sysroot:
  extends:
    - .ospack-build
    - .arm64
    - .sysroot

build-amd64-sysroot:
  extends:
    - .ospack-build
    - .amd64
    - .sysroot

######
# stage: upload

upload-armhf-rfs:
  extends:
    - .artifacts-upload
    - .armhf
    - .rfs
  needs:
    - prepare-build-env
    - build-armhf-rfs

upload-arm64-rfs:
  extends:
    - .artifacts-upload
    - .arm64
    - .rfs
  needs:
    - prepare-build-env
    - build-arm64-rfs

upload-amd64-rfs:
  extends:
    - .artifacts-upload
    - .amd64
    - .rfs
  needs:
    - prepare-build-env
    - build-amd64-rfs

upload-armhf-sysroot:
  extends:
    - .artifacts-upload
    - .armhf
    - .sysroot
  needs:
    - prepare-build-env
    - build-armhf-sysroot

upload-arm64-sysroot:
  extends:
    - .artifacts-upload
    - .arm64
    - .sysroot
  needs:
    - prepare-build-env
    - build-arm64-sysroot

upload-amd64-sysroot:
  extends:
    - .artifacts-upload
    - .amd64
    - .sysroot
  needs:
    - prepare-build-env
    - build-amd64-sysroot

######
# stage: submit tests

submit-tests-armhf-rfs:
  extends:
    - .submit-tests
    - .armhf
    - .rfs
  needs:
    - prepare-build-env
    - job: upload-armhf-rfs
      artifacts: false

submit-tests-arm64-rfs:
  extends:
    - .submit-tests
    - .arm64
    - .rfs
  needs:
    - prepare-build-env
    - job: upload-arm64-rfs
      artifacts: false

submit-tests-amd64-rfs:
  extends:
    - .submit-tests
    - .amd64
    - .rfs
  needs:
    - prepare-build-env
    - job: upload-amd64-rfs
      artifacts: false

